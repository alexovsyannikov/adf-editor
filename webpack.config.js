const webpack = require('webpack');

const moduleRules = [
    {
        test: /\.js$/,
        exclude: /(node_modules)/,
        use: {
            loader: 'babel-loader',
            options: {
                presets: ['@babel/preset-env', '@babel/preset-react'],
                plugins: [
                    '@babel/plugin-proposal-class-properties',
                    '@babel/plugin-proposal-object-rest-spread',
                    '@babel/plugin-transform-arrow-functions',
                    '@babel/plugin-transform-runtime'
                ]
            }
        }
    },
    {
        test: /\.css$/i,
        use: ['style-loader', 'css-loader'],
    },
];

module.exports = [{
    entry: {
        'adf-editor': './src/main/resources/js/adf-editor/index.js',
    },
    output: {
        filename: '[name].js',
        path: __dirname + '/target/classes/static',
    },
    module: {
        rules: moduleRules
    },
    resolve: {
        alias: {
            meta$: __dirname + '/src/main/resources/js/util/meta.js',
        }
    },
    plugins: [
        new webpack.optimize.LimitChunkCountPlugin({
            maxChunks: 1
        })
    ]
}];