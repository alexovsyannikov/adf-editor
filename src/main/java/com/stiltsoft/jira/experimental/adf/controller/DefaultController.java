package com.stiltsoft.jira.experimental.adf.controller;

import com.atlassian.connect.spring.AtlassianHostUser;
import org.springframework.boot.context.properties.bind.DefaultValue;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.InputStream;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.springframework.util.MimeTypeUtils.APPLICATION_JSON_VALUE;


@Controller
public class DefaultController {

    @RequestMapping(value = "/index", method = RequestMethod.GET)
    public String getIndexPage(@AuthenticationPrincipal AtlassianHostUser hostUser, Model model) {

        String userAccountId = getUserAccountId(hostUser);
        String hostUrl = getUserHostUrl(hostUser);

        model.addAttribute("hostUrl", hostUrl);
        return "index";
    }

    @ResponseBody
    @RequestMapping(value = "/emoji/standard", method = RequestMethod.GET, produces = APPLICATION_JSON_VALUE)
    public String getFieldAtlassianDocumentFormatDialogPage(@RequestParam(name = "altScale") @DefaultValue("XHDPI") String altScale) throws JsonNotFoundException {
        Resource resource = new ClassPathResource("/emoji/emoji_" + altScale + ".json");
        try(InputStream stream = resource.getInputStream()) {
            return StreamUtils.copyToString(stream, UTF_8);
        } catch (Throwable throwable) {
            throw new JsonNotFoundException(throwable.getMessage(), throwable);
        }
    }

    private String getUserHostUrl(AtlassianHostUser hostUser) {
        return hostUser.getHost().getBaseUrl();
    }

    private String getUserAccountId(AtlassianHostUser hostUser) {
        return hostUser.getUserAccountId().orElse("anonymous");
    }
}