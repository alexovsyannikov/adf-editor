class MetaManager {

    metaDictionary = {};

    constructor() {
        const metas = document.getElementsByTagName('meta');

        for (let i = 0; i < metas.length; i++) {
            const meta = metas[i];
            this.metaDictionary[meta.getAttribute('name')] = meta.getAttribute('content');
        }
    }

    get = (key) => {
        const value = this.metaDictionary[key];
        return value ? value : '';
    }
}

const meta = new MetaManager();

export default meta;