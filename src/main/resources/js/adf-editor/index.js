import React from 'react';
import ReactDOM from 'react-dom';
import EditorADF from "./components/EditorADF";

window.onload = () => {
    ReactDOM.render(<EditorADF/>, document.getElementById('app-container'));
};