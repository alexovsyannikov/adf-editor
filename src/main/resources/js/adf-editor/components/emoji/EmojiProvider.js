import meta from "meta";
import {EmojiResource} from "@atlaskit/emoji/resource";

const emojiProvider = new EmojiResource({
    providers: [{
        url: '/emoji/standard',
        securityProvider: () => ({
            headers: {
                Authorization: 'JWT ' + meta.get('token'),
            },
        }),
    }]
});

export default emojiProvider;