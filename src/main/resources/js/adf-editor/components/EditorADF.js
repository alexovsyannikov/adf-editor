import React from "react";
import {ReactRenderer} from "@atlaskit/renderer";
import jiraCardClient from "./smart-card/JiraCardClient";
import {Editor, EditorContext} from '@atlaskit/editor-core';
import {JIRATransformer} from "@atlaskit/editor-jira-transformer";
import {JSONTransformer} from "@atlaskit/editor-json-transformer";
import {Provider as SmartCardProvider} from "@atlaskit/smart-card";

import emojiProvider from "./emoji/EmojiProvider";
import mentionProvider from "./mention/MentionProvider";
import jiraCardProvider from "./smart-card/JiraCardProvider";
import Container, {Item} from "./styled/Container";

class EditorADF extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            userAccountId: '',
            value: {type: "doc", version: 1, content: []}, //{...initialValue}
        };

        this.editorTransformer = new JSONTransformer();
        this.emojiProvider = Promise.resolve(emojiProvider);
        this.mentionProvider = Promise.resolve(mentionProvider);
        this.jiraCardProvider = Promise.resolve(jiraCardProvider);
    };

    componentDidMount() {
        this.getCurrentUser()
            .then(userAccountId => this.setState({userAccountId, isLoading: false}, () => {
                mentionProvider.setCurrentUserAccountId(this.state.userAccountId);
            }));
    };

    getCurrentUser = () => new Promise(resolve => {
        AP.user.getCurrentUser(user => resolve(user.atlassianAccountId));
    });

    handleChangeValue = (editorView) => {
        const value = this.editorTransformer.encode(editorView.state.doc);
        console.log(JSON.stringify(value));
        this.setState({value});
    };

    render() {
        const {isLoading, value} = this.state;

        if (isLoading) {
            return (<div>Loading...</div>);
        }

        return (
            <Container>
                <Item>
                    <h2>Editor</h2>
                    <EditorContext>
                        <SmartCardProvider client={jiraCardClient}>
                            <Editor appearance="full-width"
                                    allowRule
                                    allowDate
                                    allowPanel
                                    allowStatus
                                    allowTables
                                    allowJiraIssue
                                    allowTextColor
                                    defaultValue={value}
                                    allowKeyboardAccessibleDatepicker
                                    onChange={this.handleChangeValue}
                                    emojiProvider={this.emojiProvider}
                                    mentionProvider={this.mentionProvider}
                                    UNSAFE_cards={{provider: this.jiraCardProvider}}
                                    contentTransformerProvider={schema => new JIRATransformer(schema)}
                            />
                        </SmartCardProvider>
                    </EditorContext>
                </Item>

                <Item>
                    <h2>Preview</h2>
                    <SmartCardProvider client={jiraCardClient}>
                        <ReactRenderer truncated
                                       maxHeight={300}
                                       document={value}
                                       fadeOutHeight={30}
                        />
                    </SmartCardProvider>
                </Item>
            </Container>
        );
    };
}

export default EditorADF;