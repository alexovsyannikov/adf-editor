import styled from "styled-components";

const Container = styled.div`
    display: flex;
`;

export const Item = styled.div`
    width: 600px;
    height: 800px;
    border: 1px solid #ccc;
`;

export default Container;