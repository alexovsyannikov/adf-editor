import React from 'react';
import {Client} from '@atlaskit/smart-card';

const JIRA_CUSTOM_TASK_TYPE = "JiraCustomTaskType";
const JIRA_GENERATOR_ID = "https://www.atlassian.com/#Jira";

const jiraUrlMatch = /(https?\:\/\/[a-zA-Z0-9]+\.atlassian\.net)\/browse\/(.*)/i;

const getAppearance = (statusCategory) => {
    const {colorName} = statusCategory;

    switch (colorName) {
        case 'yellow':
            return 'inprogress';
        case 'green':
            return 'success';
        default:
            return 'default';
    }
}

class JiraCardClient extends Client {
    fetchData(url) {
        const matched = url.match(jiraUrlMatch);
        if (!matched || matched.length < 3) {
            return super.fetchData(url);
        }

        const issueHostUrl = matched[1];
        const issueKey = matched[2];

        return new Promise((resolve, reject) => {
            AP.request({
                url: `/rest/api/3/issue/${issueKey}`,
                type: 'GET',
                success: response => {
                    const issue = JSON.parse(response);
                    const issueRestURL = issue.self;

                    if (!issueRestURL.startsWith(issueHostUrl)) {
                        reject(`An issue host url is different from current. host(${issueHostUrl}) found(${issueRestURL})`);
                    }

                    const taskTypeIcon = issue.fields.issuetype.iconUrl;
                    const taskTypeName = issue.fields.issuetype.name;
                    const summary = issue.fields.summary;
                    const status = issue.fields.status.name;
                    const statusAppearance = getAppearance(issue.fields.status.statusCategory)

                    resolve({
                        meta: {
                            visibility: 'restricted',
                            access: 'granted',
                            auth: [],
                            definitionId: 'jira-native-resolve',
                        },
                        data: {
                            '@context': {
                                '@vocab': 'https://www.w3.org/ns/activitystreams#',
                                atlassian: 'https://schema.atlassian.com/ns/vocabulary#',
                                schema: 'http://schema.org/',
                            },
                            '@type': ['Object', 'atlassian:Task'],

                            icon: {
                                '@type': 'Image',
                                url: taskTypeIcon,
                            },

                            generator: {
                                '@type': 'Image',
                                '@id': JIRA_GENERATOR_ID,
                                name: 'icon',
                                icon: taskTypeIcon,
                            },

                            'atlassian:taskType': {
                                '@type': ['Object', 'atlassian:TaskType'],
                                '@id': `https://www.atlassian.com/#${JIRA_CUSTOM_TASK_TYPE}`,
                                name: `${taskTypeName}`,
                                icon: {
                                    '@type': 'Image',
                                    url: taskTypeIcon,
                                },
                            },

                            name: `${issue.key}: ${summary}`,
                            tag: {
                                '@type': 'Object',
                                name: status,
                                appearance: statusAppearance
                            },
                            url,
                        },
                    });

                },
                error: response => {
                    console.error(response);
                    reject(response);
                },
            });
        });
    }
}

const jiraCardClient = new JiraCardClient();

export default jiraCardClient;