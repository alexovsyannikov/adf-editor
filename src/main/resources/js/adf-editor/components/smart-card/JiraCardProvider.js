import React from 'react';
import {EditorCardProvider} from '@atlaskit/smart-card';

const jiraUrlMatch = /https?\:\/\/[a-zA-Z0-9]+\.atlassian\.net\/browse\//i;

class JiraCardProvider extends EditorCardProvider {
    /**
     * This method must resolve to a valid ADF that will be used to
     * replace a blue link after user pastes URL.
     *
     * @param url The pasted URL
     * @param appearance Appearance requested by the Editor
     */
    async resolve(url, appearance) {
        if (url.match(jiraUrlMatch)) {
            return {
                type: 'inlineCard',
                attrs: {url},
            };
        }

        return super.resolve(url, appearance);
    }
}

const jiraCardProvider = new JiraCardProvider();

export default jiraCardProvider;