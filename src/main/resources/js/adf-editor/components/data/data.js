export const initialValue = {
    "version": 1,
    "type": "doc",
    "content": [{
        "type": "paragraph",
        "content": [{
            "type": "inlineCard",
            "attrs": {"url": "https://aovsyannikov.atlassian.net/browse/SPE-2"}
        }, {"type": "text", "text": " "}]
    }, {
        "type": "paragraph",
        "content": [{
            "type": "mention",
            "attrs": {
                "id": "557058:795772ff-8db6-4b7d-85d6-31163e3413c9",
                "text": "@Alexander Ovsyannikov",
                "accessLevel": ""
            }
        }, {"type": "text", "text": " "}]
    }, {
        "type": "paragraph",
        "content": [{
            "type": "emoji",
            "attrs": {"shortName": ":grinning:", "id": "1f600", "text": "😀"}
        }, {"type": "text", "text": " "}]
    }]
};