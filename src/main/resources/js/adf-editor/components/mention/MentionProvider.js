class MentionProvider {

    constructor() {
        this.changeListeners = new Map();
        this.errorListeners = new Map();
        this.currentUserAccountId = '';
    }

    subscribe(key, mentionResultCallback, errorCallback) {
        if (mentionResultCallback) {
            this.changeListeners.set(key, mentionResultCallback);
        }
        if (errorCallback) {
            this.errorListeners.set(key, errorCallback);
        }
    }

    unsubscribe(key) {
        this.changeListeners.delete(key);
        this.errorListeners.delete(key);
    }

    filter = (query) => {
        AP.request({
            url: '/rest/api/3/user/search',
            type: 'GET',
            data: {
                query,
            },
            success: response => {
                const usersResponse = JSON.parse(response);

                const users = usersResponse
                    .filter(user => user.accountType === 'atlassian')
                    .map(user => ({
                        id: user.accountId,
                        avatarUrl: user.avatarUrls['32x32'],
                        name: user.displayName,
                        mentionName: user.displayName,
                        nickname: user.displayName,
                    }));

                this.changeListeners.forEach(listener => listener(users, query));
            },
            error: console.error,
        });

        return Promise.resolve();
    };

    recordMentionSelection = (mention) => {

    };

    shouldHighlightMention = (mention) => {
        return mention.id === this.currentUserAccountId
    }

    isFiltering = (query) => {
        return false;
    }

    setCurrentUserAccountId = (userAccountId) => {
        this.currentUserAccountId = userAccountId;
    }
}

const mentionProvider = new MentionProvider();

export default mentionProvider;